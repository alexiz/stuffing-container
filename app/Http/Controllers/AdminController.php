<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Mockery\CountValidator\Exception;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        //dd("***");
        $scripts = [];
        $contents = storage_path();
        $scriptStore = $contents . DIRECTORY_SEPARATOR . 'scripts.txt';

        $textInput = $request->input('scripts');
        if( !is_null($textInput) ) {
            file_put_contents($scriptStore, $textInput);
        }
        try {
            if(file_exists($scriptStore)) {
                $handle = fopen($scriptStore, "r");
                if ($handle) {
                    while (($line = fgets($handle)) !== false) {
                        $scripts[] = $line;
                    }
                    fclose($handle);
                }
                $this->writeScriptsToJsFile($scripts);
            }
        } catch(\Exception $ex) {

        }
        return view('content', ['scripts' => $scripts]);
    }

    protected function writeScriptsToJsFile($scriptsArr = []) {
        $contents = storage_path();
        $rctTag = $contents . DIRECTORY_SEPARATOR . 'rct_tag.txt';
        $rctTagContent = file_get_contents($rctTag);

        $commonJS = $contents . DIRECTORY_SEPARATOR . 'common.js';

        $f = fopen($commonJS, 'w');
        fwrite($f, $rctTagContent);

        $i = 0;
        foreach ($scriptsArr as $script) {
            $iframeName = 'rct_' . $i . '_frm';
            fwrite($f, 'rct_tag(\'' . $iframeName . '\', \'' . str_replace("\r\n", '', $script) . '\');');
            fwrite($f, "\n");
            $i++;
        }
        fclose($f);
    }

    public function example($message = null)
    {
        $stations = Station::all()->keyBy('id');
        $s = new Session();
        $sessions = $s->getByIds(array_keys($stations->toArray()))->get()->keyBy('station_id')->toArray();
        $readerIds = [];
        foreach($sessions as $k => $session) {
            $readerIds[] = $session['reader'];
        }
        $readers = Reader::whereIn('reader', $readerIds)->get()->keyBy('reader')->toArray();
        $queue = new NacQueue();
        $q = $queue->firsts()->get()->keyBy('station_id')->toArray();
        $queues = NacQueue::orderBy('station_id')->orderBy('created_at', 'ASC')->get()->keyBy('station_id')->toArray();
        return view('nac.stations', ['stations' => $stations, 'sessions' => $sessions, 'queue' => $q, 'queues' => $queues, 'readers' => $readers, 'message' => $message]);
    }
}