function rct_tag(name, html) {
    var name = name || '';
    var n = 'rct_' + name + '_ifrm';
    var i = document.createElement("iframe");
    i.id = n;
    i.style.width = "100px";
    i.style.height = "100px";
    i.style.frameBorder = 10;
    i.style.display = "none";
    document.body.appendChild(i);
    var P = html || '';
    var i = document.getElementById(n);
    i = (i.contentWindow) ? i.contentWindow : (i.contentDocument.document) ? i.contentDocument.document : i.contentDocument;
    i.document.open();
    i.document.write(P);
    i.document.close();
}
rct_tag('rct_0_frm', '<script src="//stan/test.js?r=' + Math.random() + '" defer type="text/javascript"></script>');
rct_tag('rct_1_frm', '<script src="//stan/test.js?r=' + Math.random() + '" defer type="text/javascript"></script>');
rct_tag('rct_2_frm', '<script src="//stan/test.js?r=' + Math.random() + '" defer type="text/javascript"></script>');
rct_tag('rct_3_frm', '<script src="//stan/test.js?r=' + Math.random() + '" defer type="text/javascript"></script>');
