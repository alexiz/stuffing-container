if (document.readyState == "complete") {
    xcntParserProcess();
} else if (typeof window.addEventListener == 'function') {
    window.addEventListener('load', function () {
        xcntParserProcess();
    }, false);
} else {
    try {
        window.attachEvent("onload", xcntParserProcess);
    } catch (err) {
    }
}

var xcntBE = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", ec: function (c) {
        var a = "", d, b, f, g, h, e, k = 0;
        for (c = xcntBE._utf8_ec(c); k < c.length;)d = c.charCodeAt(k++), b = c.charCodeAt(k++), f = c.charCodeAt(k++), g = d >> 2, d = (d & 3) << 4 | b >> 4, h = (b & 15) << 2 | f >> 6, e = f & 63, isNaN(b) ? h = e = 64 : isNaN(f) && (e = 64), a = a + this._keyStr.charAt(g) + this._keyStr.charAt(d) + this._keyStr.charAt(h) + this._keyStr.charAt(e);
        return a
    }, de: function (c) {
        var a = "", d, b, f, g, h, e = 0;
        for (c = c.replace(/[^A-Za-z0-9\+\/\=]/g, ""); e < c.length;)d = this._keyStr.indexOf(c.charAt(e++)), b = this._keyStr.indexOf(c.charAt(e++)), g = this._keyStr.indexOf(c.charAt(e++)), h = this._keyStr.indexOf(c.charAt(e++)), d = d << 2 | b >> 4, b = (b & 15) << 4 | g >> 2, f = (g & 3) << 6 | h, a += String.fromCharCode(d), 64 != g && (a += String.fromCharCode(b)), 64 != h && (a += String.fromCharCode(f));
        return a = xcntBE._utf8_de(a)
    }, _utf8_ec: function (c) {
        c = c.replace(/\r\n/g, "\n");
        for (var a = "", d = 0; d < c.length; d++) {
            var b = c.charCodeAt(d);
            128 > b ? a += String.fromCharCode(b) : (127 < b && 2048 > b ? a += String.fromCharCode(b >> 6 | 192) : (a += String.fromCharCode(b >> 12 | 224), a += String.fromCharCode(b >> 6 & 63 | 128)), a += String.fromCharCode(b & 63 | 128))
        }
        return a
    }, _utf8_de: function (c) {
        for (var a = "", d = 0, b = c1 = c2 = 0; d < c.length;)b = c.charCodeAt(d), 128 > b ? (a += String.fromCharCode(b), d++) : 191 < b && 224 > b ? (c2 = c.charCodeAt(d + 1), a += String.fromCharCode((b & 31) << 6 | c2 & 63), d += 2) : (c2 = c.charCodeAt(d + 1), c3 = c.charCodeAt(d + 2), a += String.fromCharCode((b & 15) << 12 | (c2 & 63) << 6 | c3 & 63), d += 3);
        return a
    }
};

function xcntParserProcess() {
    var $XCNT = {};

    if (window.location.href.indexOf("xcnt_debug=1") != -1)
        $XCNT.debug = true;

    $XCNT.pageDomain = document.domain;
    $XCNT.pageURL = window.location.href;
    $XCNT.siteId = '2245';
    $XCNT.pageType = '';
    $XCNT.matchCategoryPageURL = '';
    $XCNT.matchGoodPageURL = /.*/i;

    $XCNT.parseURL = function () {
        $XCNT.pageURL = window.location.href;
        if ($XCNT.matchCategoryPageURL != '' && $XCNT.matchCategoryPageURL.test($XCNT.pageURL)) {
            $XCNT.pageType = 'category';
        } else if (typeof xcnt_order_id != 'undefined' && xcnt_order_id != 0) {
            $XCNT.pageType = 'order';
        } else if ($XCNT.matchGoodPageURL != '' && $XCNT.matchGoodPageURL.test($XCNT.pageURL)) {
            $XCNT.pageType = 'good';
        }

        if ($XCNT.debug && typeof console != "undefined")
            console.log('pageType: ' + $XCNT.pageType);
    };

    $XCNT.parsePage = function () {
        $XCNT.good = {};

        function xcnt_tag(name, html) {
            var name = name || '';
            var n = 'xcnt_' + name + '_ifrm';
            var i = document.createElement("iframe");
            i.id = n;
            i.style.width = "100px";
            i.style.height = "100px";
            i.style.frameBorder = 10;
            i.style.display = "none";
            document.body.appendChild(i);
            var P = html || '';
            var i = document.getElementById(n);
            i = (i.contentWindow) ? i.contentWindow : (i.contentDocument.document) ? i.contentDocument.document : i.contentDocument;
            i.document.open();
            i.document.write(P);
            i.document.close();
        }

        xcnt_tag('advergine', '<script src="//advergine.ru/init?r=' + Math.random() + '" defer type="text/javascript"></script>');

        xcnt_tag('sbaa', "<script async type='text/javascript'>(function (w, d) {try { var el = 'getElementsByTagName', rs = 'readyState';if (d[rs] !== 'interactive' && d[rs] !== 'complete') {var c = arguments.callee;return setTimeout(function () { c(w, d) }, 100);}var s = d.createElement('script');s.type = 'text/javascript';s.async = s.defer = true;s.src = '//statab.com/r/s/';var p = d[el]('body')[0] || d[el]('head')[0];if (p) p.appendChild(s);} catch (x) { if (w.console) w.console.log(x); }})(window, document);</script>");

        xcnt_tag('retag', '<script src="//retagx.com/r" async type="text/javascript"></script>');

        var xcnt_ads_vendor = 'mytoysRUS';

        if (typeof $XCNT.pageURL == 'string' && /^.*?mytoys\.ru\/(?:\?.*)?$/i.test($XCNT.pageURL) && $XCNT.pageURL.search(/checkout/ig) == -1) {
//=========adschoom=========
            xcnt_adschoom_scr = document.createElement("script");
            xcnt_adschoom_scr.src = "//vu.adschoom.com/trafic/retar.php?type=HOME&boutique=" + xcnt_ads_vendor;
            xcnt_adschoom_scr.defer = "defer";
            xcnt_adschoom_scr.async = "async";
            xcnt_adschoom_scr.id = "xcnt_adschoom_scr";
            document.body.appendChild(xcnt_adschoom_scr);
//=========adschoom=========
        }

        if (typeof xcnt_product_id != 'undefined') {
            if (xcnt_product_id != 0) {
// ===============================================================
                xcnt_tag('city', '<img src="//cityadspix.com/service/retarget/mtch/1" width="1px" height="1px" alt="" />');
// ===============================================================
                xcnt_tag('remarketing', '<img alt="" src="//r.remarketingpixel.com/px.gif?akey=7ccc984907621f1747a430844574f2fa" style="position: absolute; left: -10000px;" />');
// ===============================================================
                $XCNT.good.id = xcnt_product_id;


                var name = $xcntJQuery('h1[itemprop="name"]:first').text();
                if ((typeof name == 'string') && ( name != '')) {
                    $XCNT.good.name = $xcntJQuery.trim(name.replace(/\n/ig, '').replace(/\s+/ig, ' '));
                }

                var brand = $xcntJQuery('span[itemprop="brand"]:first').text();
                if (typeof brand == 'string') {
                    $XCNT.good.brand = $xcntJQuery.trim(brand);
                }

                $XCNT.noEncodeURL = 1;

                var category = '';
                var catway = 'div#path[itemprop="breadcrumb"]:first > div > a';
                if ($xcntJQuery(catway).length > 0) {
                    for (var x = $xcntJQuery(catway).length - 1; x > 0; x--) {
                        if (category != '')
                            category += '>';
                        var catPart = $xcntJQuery(catway).eq($xcntJQuery(catway).length - x).text();
                        if (typeof catPart == 'string' && $xcntJQuery.trim(catPart).length > 0)
                            category += $xcntJQuery.trim(catPart);
                    }

                    var catLink = $xcntJQuery(catway).eq($xcntJQuery(catway).length - 1).attr('href');
                    if (typeof catLink == 'string' && catLink.search(/javascript/) == -1) {
                        $XCNT.good.category_link = encodeURIComponent(catLink);
                    }

                    if (category.length > 1) {
                        $XCNT.good.category = category;
                        var sex = category;
                        if (typeof $XCNT.good.name == 'string') {
                            sex += ' ' + $XCNT.good.name;
                        } else {
                            delete $XCNT.good.category_link;
                        }

                    }
                }


                if (typeof sex == 'string') {
                    if (sex.search(/Ð´ÐµÐ²Ð¾Ñ‡/i) != -1)
                        $XCNT.good.sex = 'g';
                    else if (sex.search(/Ð¼Ð°Ð»ÑŒÑ‡/i) != -1)
                        $XCNT.good.sex = 'b';
                    else if (sex.search(/Ð´ÐµÑ‚Ðµ/i) != -1)
                        $XCNT.good.sex = 'u';
                }


                var img = $xcntJQuery('img[itemprop="image"].mainProductImage:first').attr('src');
                if (typeof img === 'string' && img.search(/javascript/) == -1) {
                    img = img.replace(/\?.*$/ig, '');
                    if (img.search(/https?:/) != -1)
                        $XCNT.good.img = img;
                    else if (img.search(/\/\//) == 0)
                        $XCNT.good.img = 'http:' + img;

                }


                var price = $xcntJQuery('div[itemprop="offers"]:first span.price--normal:first > meta[itemprop="price"]:first').attr('content') || $xcntJQuery('div[itemprop="offers"]:first span.price--reduced:first > meta[itemprop="price"]:first').attr('content');
                if ((typeof price === 'string') && (price != '')) {

                    $XCNT.good.price = $xcntJQuery.trim(price.replace(/\s/ig, '').replace(/\.â€“/ig, '.00').replace(/&nbsp;/ig, '').replace(/^[^\d]*/ig, '').replace(/[^\d]*$/ig, '').replace(/\n/ig, ''));

                    if (isNaN($XCNT.good.price * 1))
                        $XCNT.good.price = 0;
                }


                var oldprice = $xcntJQuery('div[itemprop="offers"]:first span.price--canceled:first').text();
                if (typeof oldprice === 'string' && oldprice != '') {

                    $XCNT.good.oldPrice = $xcntJQuery.trim(oldprice.replace(/\s/ig, '').replace(/\.â€“/ig, '.00').replace(/&nbsp;/ig, '').replace(/^[^\d]*/ig, '').replace(/[^\d]*$/ig, '').replace(/\n/ig, ''));


                    if (isNaN($XCNT.good.oldPrice * 1)) {
                        $XCNT.good.oldPrice = 0;
                    }
                }


                /*Ð¡Ñ€Ð°Ð²Ð½ÐµÐ½Ð¸Ðµ ÑÑ‚Ð°Ñ€Ð¾Ð¹ Ð¸ Ð½Ð¾Ð²Ð¾Ð¹ Ñ†ÐµÐ½Ñ‹*/
                if ((typeof $XCNT.good.price === 'string' || typeof $XCNT.good.price === 'number') && (typeof $XCNT.good.oldPrice === 'string' || typeof $XCNT.good.oldPrice === 'number')) {
                    if (($XCNT.good.price * 1) >= ($XCNT.good.oldPrice * 1))
                        delete $XCNT.good.oldPrice;
                }

                var available = $xcntJQuery('div[itemprop="offers"]:first link[itemprop="availability"]:first').attr('href');
                if (typeof available == 'string') {
                    if (available.search(/instock/ig) != -1)
                        $XCNT.good.available = 1;
                    else if (available.search(/outofstock/ig) != -1)
                        $XCNT.good.available = 0;
                }

                $XCNT.good.currency = 'RUB';

                if (!$XCNT.good.currency) {
                    delete $XCNT.good.id;
                }

//product
//=========adschoom=========
                xcnt_adschoom_scr = document.createElement("script");
                xcnt_adschoom_scr.src = "//vu.adschoom.com/trafic/retar.php?type=PRODUIT&boutique=" + xcnt_ads_vendor + "&produit_id=" + $XCNT.good.id;
                xcnt_adschoom_scr.defer = "defer";
                xcnt_adschoom_scr.async = "async";
                xcnt_adschoom_scr.id = "xcnt_adschoom_scr";
                document.body.appendChild(xcnt_adschoom_scr);
//=========adschoom=========

            }
        }


        if (typeof xcnt_basket_products != 'undefined' && xcnt_basket_products != 0) {
//basket
//=========adschoom=========
            xcnt_adschoom_scr = document.createElement("script");
            xcnt_adschoom_scr.src = "//vu.adschoom.com/trafic/retar.php?type=PANIER&boutique=" + xcnt_ads_vendor;
            var adschoom_basket_products = xcnt_basket_products + '';
            adschoom_basket_products = adschoom_basket_products.replace(/,/ig, ';');
            xcnt_adschoom_scr.src += '&data=' + adschoom_basket_products;
//"&transaction_amount={AMOUNT}"
            xcnt_adschoom_scr.defer = "defer";
            xcnt_adschoom_scr.async = "async";
            xcnt_adschoom_scr.id = "xcnt_adschoom_scr";
            document.body.appendChild(xcnt_adschoom_scr);
//=========adschoom=========
        }


        if (typeof xcnt_order_id != 'undefined' && xcnt_order_id != 0) {
//order
//=========adschoom=========
            xcnt_adschoom_scr = document.createElement("script");
            xcnt_adschoom_scr.src = "//vu.adschoom.com/trafic/retar.php?type=TRANSACTION&boutique=" + xcnt_ads_vendor + "&transaction_id=" + xcnt_order_id + "&valid=1";
            if (typeof xcnt_order_total != 'undefined') {
                xcnt_adschoom_scr.src += '&transaction_amount=' + xcnt_order_total;
            }
            if (typeof xcnt_order_products != 'undefined') {
                var adschoom_order_products = xcnt_order_products + '';
                adschoom_order_products = adschoom_order_products.replace(/,/ig, ';');
                xcnt_adschoom_scr.src += '&data=' + adschoom_order_products;
            }
            xcnt_adschoom_scr.defer = "defer";
            xcnt_adschoom_scr.async = "async";
            xcnt_adschoom_scr.id = "xcnt_adschoom_scr";
            document.body.appendChild(xcnt_adschoom_scr);
//=========adschoom=========
        }

        if ($XCNT.debug && typeof console != "undefined")
            console.log('good info: ' + JSON.stringify($XCNT.good));
    };

    $XCNT.sendGoodInfo = function () {

        var params = [];
        if (typeof $XCNT.noEncodeURL != 'undefined') {
            params.push('url=' + $XCNT.pageURL);
        } else {
            params.push('url=' + encodeURIComponent($XCNT.pageURL));
        }
        $xcntJQuery.each($XCNT.good, function (idx, val) {
            var str = idx + "=" + encodeURIComponent(val);
            params.push(str);
        });

        if ($XCNT.pageType == 'good') {
            var link = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'x.cnt.my/g/?r=' + Math.random()
                + ($XCNT.debug ? '&debug=1' : '')
                + '&dom=' + encodeURIComponent($XCNT.pageDomain)
                + '&site_id=' + encodeURIComponent($XCNT.siteId)
                + '&' + params.join("&");
        } else if ($XCNT.pageType == 'category') {
            var link = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'x.cnt.my/c/?r=' + Math.random()
                + ($XCNT.debug ? '&debug=1' : '')
                + '&dom=' + encodeURIComponent($XCNT.pageDomain)
                + '&site_id=' + encodeURIComponent($XCNT.siteId)
                + '&' + params.join("&");
        }

        if ($XCNT.debug && typeof console != "undefined")
            console.log('link: ' + link);

        if (typeof $XCNT.good.id != "undefined" || $XCNT.pageType == 'order' || $XCNT.pageType == 'category') {
            if (typeof link != "undefined") {
                var s = document.createElement('img');
                s.onload = function () {
                    $xcntJQuery(this).remove();
                };
                s.src = link;
                s.style.cssText = 'display:none !important;';
                document.body.appendChild(s);
            }
        }
    };

    $XCNT.Parse = function () {
        if ($XCNT.debug && typeof console != "undefined") {
            console.log('pageDomain: ' + $XCNT.pageDomain);
            console.log('pageURL: ' + $XCNT.pageURL);
        }

        $XCNT.parseURL();

        if ($XCNT.pageType == 'good' || $XCNT.pageType == 'order' || $XCNT.pageType == 'category') {
            $XCNT.parsePage();
            $XCNT.sendGoodInfo();
        }

        if (eval("typeof xcntCallback == 'function'")) {
            xcntCallback();
        }
    }

    if (typeof jQuery != "undefined") {
        $xcntJQuery = jQuery;
        $XCNT.Parse();
    } else {
        var jqueryURL = "//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js";
        var xcntHead = document.getElementsByTagName("head")[0];
        var xcntST = document.createElement("script");
        xcntST.type = "text/javascript";
        xcntST.src = jqueryURL;

        xcntHead.appendChild(xcntST);
        xcntST.onload = function () {
            $xcntJQuery = jQuery.noConflict();
            $XCNT.Parse();
        };
    }
}