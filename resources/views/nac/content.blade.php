@extends('layouts.adminlte')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Очередь
                <small>Компьютер: {{ $station->name  }}</small>
            </h1>
            <!--ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol-->
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <!-- /.row -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!--div class="box-header">
                            <h3 class="box-title">Компьютеры</h3>

                            <div class="box-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div-->
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>Читатель</th>
                                    <th>Длительность сеанса</th>
                                    <th>Коммерческий сеанс</th>
                                    <th>Дата/время созадния заявки</th>
                                    <th>Дата/время обновления заявки</th>
                                    <th>Удалить</th>
                                </tr>
                                @foreach($queue as $item)
                                    <tr>
                                        <td>{{$item->reader}}</td>
                                        <td>{{$item->period}}</td>
                                        <td>
                                            @if($item->is_commercial == 1)
                                                <small class="label label-success"><i class="fa fa-dollar"></i>платный</small>
                                            @endif
                                        </td>
                                        <td>{{$item->created_at}}</td>
                                        <td>{{$item->updated_at}}</td>
                                        <td>
                                            <a href="/nac/queue/delete/{{ $item->reader  }}/{{ $station->id }}" class="btn btn-warning btn-xs" >Удалить</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection