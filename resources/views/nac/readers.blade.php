@extends('layouts.adminlte')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Статистика по сегодняшним читателям
                <!--small>Optional description</small-->
            </h1>
            <!--ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol-->
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <!-- /.row -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!--div class="box-header">
                            <h3 class="box-title">Компьютеры</h3>

                            <div class="box-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div-->
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>Читательский билет</th>
                                    <th>ФИО</th>
                                    <th>Потраченное бесплатное время</th>
                                    <th>Потраченное платное время</th>
                                </tr>
                                @foreach($periods as $reader => $info)
                                    <tr>
                                        <td>{{$reader}}</td>
                                        <td>{{ isset($readers[$reader]) ?  $readers[$reader]['fio'] : '' }}</td>
                                        <td>{{ isset($info['free']) ? $info['free'] : 0 }}</td>
                                        <td>{{ isset($info['commercial']) ? $info['commercial'] : 0  }}</td>
                                    </tr>
                                @endforeach
                            </table>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection