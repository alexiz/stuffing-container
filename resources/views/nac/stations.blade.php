@extends('layouts.adminlte')
@section('content')
    <script>
        function uiUpdater () {
            setTimeout(function () {

                $.ajax({
                    url: "/api/sessions",
                }).done(function( data ) {

                    $.each( data['sessions'], function( key, value ) {
                        console.log( value );
                        var now = new Date();
                        var finishDateTime = new Date(value['finish_plan_at']);
                        var timeLeft = finishDateTime - now;
                        timeLeft = Math.floor(timeLeft/60/1000);
                        $("#time" + value['id']).html( timeLeft + " мин." );
                        if( value['screenshot'] != null && value['screenshot'].length > 0 ) {
                            $("#screen" + value['id']).attr("src", "/storage/" + value['screenshot']);
                            $("#bigscreen" + value['id']).attr("src", "/storage/" + value['screenshot']);
                        } else {
                            $("#screen" + value['id']).attr("src", 'http://placehold.it/150x100');
                            $("#bigscreen" + value['id']).attr("src", 'http://placehold.it/150x100');
                        }
                    });
                });

                uiUpdater();
            }, 20000)
        }

        uiUpdater();
    </script>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Компьютеры
                <!--small>Optional description</small-->
            </h1>
            <!--ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol-->
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <!-- /.row -->
            <div class="row">
                <div class="col-xs-12">
                    @if( !is_null($message) )
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-warning"></i> Внимание!</h4>
                            @switch($message)
                                @case('reader_not_exists')
                                Пользователь не зарегистрирован в Opac-Global
                                @break
                                @case('reader_disabled')
                                Читатель заблокирован или не прошел перерегистрацию
                                @break
                                @case('already_exists')
                                Пользователь уже находится в очереди на один из компьютеров или работает за компьютером прямо сейчас
                                @break
                                @case('time_over')
                                Бесплатное время исчерпано
                                @break
                                @case('exception')
                                Непредвиденная ошибка. Сообщите в отдел разработки.
                                @break

                            @endswitch

                        </div>
                    @endif
                    <div class="box">

                        <!--div class="box-header">
                            <h3 class="box-title">Компьютеры</h3>

                            <div class="box-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div-->
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>ID</th>
                                    <th>Компьютер</th>
                                    <th>Скриншот</th>
                                    <th>Статус</th>
                                    <th>Время до завершения сеанса</th>
                                    <th>Сообщение</th>
                                    <th>Очередь</th>
                                    <th>Записать</th>
                                    <th>Завершить сеанс</th>
                                </tr>
                                @foreach($stations as $station)
                                    <tr>
                                        <td>{{$station->id}}</td>
                                        <td>{{$station->name}}</td>
                                        <td>
                                            <img id="screen{{ isset($sessions[$station->id]['id']) ? $sessions[$station->id]['id'] : ''  }}" src="{{ isset($sessions[$station->id]['screenshot']) ? '/storage/' . $sessions[$station->id]['screenshot'] : 'http://placehold.it/150x100' }}" alt="" width="150px" class="margin" data-toggle="modal" data-target="#modal-image{{$station->id}}">
                                            <div class="modal modal-info fade" id="modal-image{{$station->id}}" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                            <h4 class="modal-title">Экран</h4>
                                                        </div>
                                                        <div class="modal-body">

                                                            <img id="bigscreen{{ isset($sessions[$station->id]['id']) ? $sessions[$station->id]['id'] : ''  }}" src="{{ isset($sessions[$station->id]['screenshot']) ? '/storage/' . $sessions[$station->id]['screenshot'] : 'http://placehold.it/150x100' }}" alt="" class="margin" >

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                        </td>
                                        @if( isset($sessions[$station->id]) )
                                            <td>
                                                <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal-reader{{$station->id}}">Занят - {{ $sessions[$station->id]['reader'] }}</button>
                                                <div class="modal modal-warning fade" id="modal-reader{{$station->id}}" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title">Читатель: {{ $sessions[$station->id]['reader'] }}</h4>
                                                            </div>
                                                            <div class="modal-body">

                                                                <h3>Читательский билет: {{ $sessions[$station->id]['reader']  }}</h3>
                                                                <h3>ФИО: {{ isset($readers[$sessions[$station->id]['reader']]) ?  $readers[$sessions[$station->id]['reader']]['fio'] : '' }}</h3>
                                                                <h3>Login: {{ isset($readers[$sessions[$station->id]['reader']]) ?  $readers[$sessions[$station->id]['reader']]['login'] : ''  }}</h3>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                @if($sessions[$station->id]['is_commercial'] == 1)
                                                    <span class="label label-success">Платный</span>
                                                @endif
                                            </td>
                                            @php
                                                $finishTime = new \DateTime($sessions[$station->id]['finish_plan_at']);
                                                $current = new \DateTime();
                                                $interval = $finishTime->diff($current);
                                                $hours = $interval->format('%h');
                                                $minutes = $interval->format('%i');
                                                $timeLeft = $minutes + ($hours * 60);
                                                if((int)$timeLeft > 0) {
                                                    $timeLeft .= ' мин.';
                                                }
                                            @endphp
                                            <td>
                                                <b id="time{{ $sessions[$station->id]['id'] }}">{{ $timeLeft }}</b>
                                                <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modal-prolong{{$station->id}}">Продлить</button>
                                                <div class="modal fade" id="modal-prolong{{$station->id}}" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title">Компьютер: {{$station->name}}</h4>
                                                            </div>
                                                            <div class="modal-body">

                                                                <div class="box box-primary">
                                                                    <div class="box-header with-border">
                                                                        <h3 class="box-title">Платно продлить сеанс читателя {{ $sessions[$station->id]['reader'] }}</h3>
                                                                    </div>
                                                                    <!-- /.box-header -->
                                                                    <!-- form start -->
                                                                    <form role="form" action="/nac/station/prolong" method="POST">
                                                                        <div class="box-body">
                                                                            {!! csrf_field() !!}
                                                                            <input name="session_id" type="hidden" value="{{$sessions[$station->id]['id']}}">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputReader">Количество платных часов</label>
                                                                                <input name="period" type="text" class="form-control" id="exampleInputReader" placeholder="Количество платных часов">
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.box-body -->

                                                                        <div class="box-footer">
                                                                            <input type="submit" class="btn btn-primary"/>
                                                                        </div>
                                                                    </form>
                                                                </div>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                            </td>
                                            <td>
                                                @php
                                                    $isRead = 'primary';
                                                    $title = 'Написать';
                                                    if(!empty($sessions[$station->id]['message'])) {
                                                        $isRead = 'danger';
                                                        $title = 'Сообщение не прочитано';
                                                    }
                                                @endphp
                                                <button type="button" class="btn btn-{{ $isRead }} btn-xs" data-toggle="modal" data-target="#modal-message{{$station->id}}">{{ $title  }}</button>
                                                <div class="modal fade" id="modal-message{{$station->id}}" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title">Компьютер: {{$station->name}}</h4>
                                                            </div>
                                                            <div class="modal-body">


                                                                <div class="box box-primary">
                                                                    <div class="box-header with-border">
                                                                        <h3 class="box-title">Отправить сообщение читателю</h3>
                                                                    </div>
                                                                    <!-- /.box-header -->
                                                                    <!-- form start -->
                                                                    <form role="form" action="/nac/station/message" method="POST">
                                                                        <div class="box-body">
                                                                            {!! csrf_field() !!}
                                                                            <input name="session_id" type="hidden" value="{{$sessions[$station->id]['id']}}">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputReader">Сообщение</label>
                                                                                <textarea name="message" class="form-control" rows="5" id="exampleInputReader" placeholder="Текст сообщения...">{{ $sessions[$station->id]['message'] }}</textarea>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.box-body -->

                                                                        <div class="box-footer">
                                                                            <input type="submit" class="btn btn-primary"/>
                                                                        </div>
                                                                    </form>
                                                                </div>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                                                                <!--button type="button" class="btn btn-primary">Save changes</button-->
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                            </td>
                                        @endif
                                        @if( !isset($sessions[$station->id]) )
                                            <td><span class="label label-success">Свободен</span></td>
                                            <td></td>
                                            <td></td>
                                        @endif

                                        <td>
                                            @php
                                            $queueLength = 'success';
                                            if(  isset($queue[$station->id]['count']) && $queue[$station->id]['count'] > 0 ) {
                                                $queueLength = 'warning';
                                            }
                                            @endphp
                                            <a href="/nac/queue/list/{{ $station->id  }}" class="btn btn-{{ $queueLength  }} btn-xs" >Размер очереди - {{ isset($queue[$station->id]['count']) ? $queue[$station->id]['count'] : 0 }}</a>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modal-content{{$station->id}}">Записать</button>
                                            <div class="modal fade" id="modal-content{{$station->id}}" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                            <h4 class="modal-title">Компьютер: {{$station->name}}</h4>
                                                        </div>
                                                        <div class="modal-body">


                                                            <div class="box box-primary">
                                                                <div class="box-header with-border">
                                                                    <h3 class="box-title">Запись читателя на компьютер</h3>
                                                                </div>
                                                                <!-- /.box-header -->
                                                                <!-- form start -->
                                                                <form role="form" action="/nac/queue/add" method="POST">
                                                                    <div class="box-body">
                                                                        {!! csrf_field() !!}
                                                                        <input name="station_id" type="hidden" value="{{$station->id}}">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputReader">Номер читательского билета</label>
                                                                            <input name="reader" type="text" class="form-control" id="exampleInputReader" placeholder="Номер читательского билета">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="exampleInputCommercialPeriod">Платные часы</label>
                                                                            <input name="period" type="text" class="form-control" id="exampleInputCommercialPeriod" value="0" placeholder="Количество платных часов">
                                                                        </div>
                                                                    </div>
                                                                    <!-- /.box-body -->

                                                                    <div class="box-footer">
                                                                        <input type="submit" class="btn btn-primary"/>
                                                                    </div>
                                                                </form>
                                                            </div>


                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                            <!--button type="button" class="btn btn-primary">Save changes</button-->
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                        </td>
                                        <td>
                                            @if( isset($sessions[$station->id]) )
                                                <form role="form" action="/nac/session/stop" method="POST">
                                                    <input type="submit" class="btn btn-danger btn-xs" value="Стоп"/>
                                                    <input type="hidden" name="session_id" value="{{ $sessions[$station->id]['id'] }}">
                                                    {!! csrf_field() !!}
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </table>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection