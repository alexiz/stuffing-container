@extends('layouts.adminlte')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Скрипты в контейнере
                <!--small>Optional description</small-->
            </h1>
            <!--ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol-->
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <!-- /.row -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <form role="form" method="post">
                            <div class="box-body">

                                <!-- textarea -->
                                <div class="form-group">
                                    <label>Скрипты</label>
                                    <textarea name="scripts" class="form-control" rows="10" placeholder="JS scripts ...">@foreach($scripts as $script){{ $script }}@endforeach</textarea>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Записать</button>
                            </div>
                            {!! csrf_field() !!}

                        </form>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>


        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection